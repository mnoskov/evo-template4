/**
 * preview_alg
 *
 * preview_alg
 *
 * @category	tv
 * @name	preview_alg
 * @internal	@caption Тип предпросмотра в списке товаров
 * @internal	@input_type dropdown
 * @internal	@modx_category 
 * @internal	@input_default inherit
 * @internal	@input_options Как в настройках сайта==inherit||Слайдер изображений==slider||Видео==video
 */
