//<?php
/**
 * Prepare Page
 *
 * Prepare Page
 *
 * @category    plugin
 * @internal    @events OnManagerPageInit,OnWebPageInit,OnAfterLoadDocumentObject,OnPageNotFound
 * @internal    @modx_category 
 * @internal    @properties 
 * @internal    @disabled 0
 * @internal    @installset base
 */
if (!isset($modx->templateConstants)) {
	$cachename = MODX_BASE_PATH . 'assets/cache/constants.tpl4.pageCache.php';

	if (file_exists($cachename)) {
		$data = json_decode(file_get_contents($cachename), true);
	} else {
		$data = [];

		$query = $modx->db->select('templatename, id', $modx->getFullTableName('site_templates'));
		while ($row = $modx->db->getRow($query)) {
			$data[$row['templatename'] . '_template_id'] = $row['id'];
		}

		file_put_contents($cachename, json_encode($data));
	}

	$modx->templateConstants = $data;
}

switch ($modx->event->name) {
    case 'OnAfterLoadDocumentObject': {
        foreach ($modx->templateConstants as $key => $value) {
            $modx->setPlaceholder('my_' . $key, $value);
        }

        break;
    }
}

