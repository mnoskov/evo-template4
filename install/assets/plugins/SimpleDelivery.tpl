//<?php
/**
 * SimpleDelivery
 *
 * SimpleDelivery
 *
 * @category    plugin
 * @internal    @events OnCollectSubtotals,OnRegisterDelivery
 * @internal    @modx_category Commerce
 * @internal    @properties
 * @internal    @disabled 0
 * @internal    @installset base
 */
$title = 'Доставка';
$price = ci()->currency->convertToActive(300);

switch ($modx->Event->name) {
    case 'OnRegisterDelivery': {
        // Регистрация доставки
        $params['rows']['fixed'] = [
            'title' => $title,
            'price' => $price,
        ];

        break;
    }

    case 'OnCollectSubtotals': {
        $processor = $modx->commerce->loadProcessor();

        if ($processor->isOrderStarted() && $processor->getCurrentDelivery() == 'fixed') {
            // Если заказ в процессе оформления и эта доставка выбрана,
            // добавляем стоимость доставки в заказ
            $params['total'] += $price;
            $params['rows']['fixed'] = [
                'title' => $title,
                'price' => $price,
            ];
        }
        break;
    }
}
