//<?php
/**
 * Install
 *
 * Template installer
 *
 * @category    plugin
 * @author      mnoskov
 * @internal    @events OnWebPageInit,OnManagerPageInit,OnPageNotFound
 * @internal    @installset base
*/

$modx->clearCache('full');

$templates = [];
$query = $modx->db->select('*', $modx->getFullTablename('site_templates'));

while ($row = $modx->db->getRow($query)) {
    $templates[$row['templatename']] = $row['id'];
}

$tvs = [];
$query = $modx->db->select('*', $modx->getFullTablename('site_tmplvars'));

while ($row = $modx->db->getRow($query)) {
    $tvs[$row['name']] = $row['id'];
}

$content_data = [
    [
        'id' => '10',
        'pagetitle' => 'Каталог',
        'alias' => 'catalog',
        'published' => '1',
        'parent' => '1',
        'isfolder' => '1',
        'content' => '',
        'template' => $templates['catalog'],
        'menuindex' => '1',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '11',
        'pagetitle' => 'Категория 1',
        'alias' => 'kategoriya-1',
        'published' => '1',
        'parent' => '10',
        'isfolder' => '1',
        'content' => '',
        'template' => $templates['category'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '12',
        'pagetitle' => 'Категория 2',
        'alias' => 'kategoriya-2',
        'published' => '1',
        'parent' => '10',
        'isfolder' => '1',
        'content' => '',
        'template' => $templates['category'],
        'menuindex' => '1',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '13',
        'pagetitle' => 'Товар 1',
        'alias' => 'tovar-1',
        'published' => '1',
        'parent' => '11',
        'content' => '<p>Описание товара</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '14',
        'pagetitle' => 'Товар 2',
        'alias' => 'tovar-2',
        'published' => '1',
        'parent' => '11',
        'content' => '<p>Описание товара</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '15',
        'pagetitle' => 'Товар 3',
        'alias' => 'tovar-3',
        'published' => '1',
        'parent' => '11',
        'content' => '<p>Описание товара</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '16',
        'pagetitle' => 'Товар 4',
        'alias' => 'tovar-4',
        'published' => '1',
        'parent' => '12',
        'content' => '<p>Описание товара</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '17',
        'pagetitle' => 'Товар 5',
        'alias' => 'tovar-5',
        'published' => '1',
        'parent' => '12',
        'content' => '<p>Описание товара</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '18',
        'pagetitle' => 'О компании',
        'alias' => 'about',
        'published' => '1',
        'parent' => '1',
        'content' => '<p>Эксклюзивные изделия из эпоксидной смолы для сервировки стола и декора дома<br /><br />😉 Изделия, которые не найти на полках магазина<br />😍 Идеальный подарок для самых искушенных<br /><br />&bull; Все изделия покрыты защитным средством от повышенной температуры и царапин<br />&bull; Гарантия 3 месяца<br />&bull; Сразу идут в подарочной упаковке</p>',
        'template' => $templates['info'],
        'menuindex' => '2',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '19',
        'pagetitle' => 'Вопрос-ответ',
        'alias' => 'faq',
        'published' => '1',
        'parent' => '1',
        'content' => '',
        'template' => $templates['faq'],
        'menuindex' => '3',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '20',
        'pagetitle' => 'Оплата и доставка',
        'alias' => 'oplata-i-dostavka',
        'published' => '1',
        'parent' => '1',
        'content' => '<p>Изделия из наличия оплачиваются по 100% предоплате. Отправка осуществляется почтой России на следующий день после оплыты. Средний срок доставки &ndash; 3-4 рабочих дня по России.</p><p>Стоимость доставки оплачивается отдельная. Средняя цена доставки 300-350 рублей по России</p><p>Изделия под заказ создаются в течение 3-4 рабочих суток (с ПН по ПТ).</p>',
        'template' => $templates['info'],
        'menuindex' => '4',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '21',
        'pagetitle' => 'Контакты',
        'alias' => 'contacts',
        'published' => '1',
        'parent' => '1',
        'content' => '',
        'template' => $templates['contacts'],
        'menuindex' => '8',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '22',
        'pagetitle' => 'Отзывы',
        'alias' => 'otzyvy',
        'published' => '1',
        'parent' => '1',
        'isfolder' => '1',
        'content' => '',
        'template' => $templates['reviews'],
        'menuindex' => '7',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '23',
        'pagetitle' => 'Лаврин Александр',
        'alias' => 'lavrin-aleksandr',
        'published' => '1',
        'parent' => '22',
        'content' => '<p>Заказал кухню, шкаф-купе, комоды, рабочие столы в детскую комнату и кабинет. Все сделано замечательно, претензий к качеству нет.&nbsp; Немного страдают сроки. Это связано с большим спросом на услуги ребят!</p>',
        'template' => $templates['info'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '24',
        'pagetitle' => 'Сакардина Елена',
        'alias' => 'sakardina-elena',
        'published' => '1',
        'parent' => '22',
        'content' => '<p>Хочу поблагодарить вашу фирму за предоставленную услугу. Мебель, которую вы мне сделали, прекрасно вписалась в интерьер комнаты и радует меня каждый день. Спасибо огромное!</p>',
        'template' => $templates['info'],
        'menuindex' => '1',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '25',
        'pagetitle' => 'Список',
        'alias' => 'spisok',
        'published' => '1',
        'parent' => '1',
        'isfolder' => '1',
        'content' => '',
        'template' => $templates['list'],
        'menuindex' => '7',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '26',
        'pagetitle' => 'Элемент 1',
        'alias' => 'element-1',
        'published' => '1',
        'parent' => '25',
        'introtext' => 'Изделия покрыты термозащитным слоем. Не боятся высоких температур и царапин В подарочной упаковке Действует гарантия 3 месяца на прочность изделия при падении с высоты до 1м и крепления фурнитуры',
        'content' => '<p>Изделия покрыты термозащитным слоем. Не боятся высоких температур и царапин В подарочной упаковке Действует гарантия 3 месяца на прочность изделия при падении с высоты до 1м и крепления фурнитуры</p>',
        'template' => $templates['info'],
        'menuindex' => '0',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '27',
        'pagetitle' => 'Элемент 2',
        'alias' => 'element-2',
        'published' => '1',
        'parent' => '25',
        'introtext' => 'Изделия покрыты термозащитным слоем. Не боятся высоких температур и царапин В подарочной упаковке Действует гарантия 3 месяца на прочность изделия при падении с высоты до 1м и крепления фурнитуры',
        'content' => '<p>Изделия покрыты термозащитным слоем. Не боятся высоких температур и царапин В подарочной упаковке Действует гарантия 3 месяца на прочность изделия при падении с высоты до 1м и крепления фурнитуры</p>',
        'template' => $templates['info'],
        'menuindex' => '1',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
    ], [
        'id' => '28',
        'pagetitle' => 'Результаты поиска',
        'alias' => 'search',
        'published' => '1',
        'parent' => '0',
        'content' => '',
        'template' => $templates['search'],
        'menuindex' => '4',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
        'hidemenu' => '1',
    ], [
        'id' => '29',
        'pagetitle' => 'Корзина',
        'alias' => 'cart',
        'published' => '1',
        'parent' => '0',
        'content' => '',
        'template' => $templates['cart'],
        'menuindex' => '5',
        'createdby' => '1',
        'createdon' => '1631871701',
        'publishedon' => '1631871701',
        'publishedby' => '1',
        'hidemenu' => '1',
    ],
];

$site_content = $modx->getFullTablename('site_content');

foreach ($content_data as $row) {
    foreach (['pagetitle', 'longtitle', 'content', 'introtext'] as $field) {
        if (isset($row[$field])) {
            $row[$field] = $modx->db->escape($row[$field]);
        }
    }

    $modx->db->insert($row, $site_content);
}

$site_tmplvar_templates = $modx->getFullTablename('site_tmplvar_templates');
$all_templates = array_keys($templates);
$all_templates_but_main = array_diff($all_templates, ['main']);

$tmplvar_templates_data = [
    'meta_description' => $all_templates,
    'meta_keywords'    => $all_templates,
    'meta_title'       => $all_templates,
    'og_description'   => $all_templates,
    'og_image'         => $all_templates,
    'og_title'         => $all_templates,
    'image'            => $all_templates_but_main,
    'video'            => ['product', 'main'],
    'materials'        => ['product'],
    'preview_alg'      => ['product'],
    'price'            => ['product'],
    'size'             => ['product'],
    'color'            => ['product'],
    'tovarparams'      => ['catalog'],
];

foreach ($tmplvar_templates_data as $tvname => $tnames) {
    $modx->db->delete($site_tmplvar_templates, "`tmplvarid` = '" . $tvs[$tvname] . "'");

    foreach ($tnames as $tname) {
        $modx->db->insert([
            'tmplvarid'  => $tvs[$tvname],
            'templateid' => $templates[$tname],
        ], $site_tmplvar_templates);
    }
}

$tmplvar_contentvalues_data = [
    '10' => [
        'tovarparams' => $modx->db->escape(json_encode([
            'fieldValue' => [
                [
                    'param_id' => $tvs['price'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '3',
                    'fltr_name' => 'Цена, руб.',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['size'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '4',
                    'fltr_name' => 'Диагональ телевизора',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['color'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '4',
                    'fltr_name' => 'Цвет',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ]
            ],
            'fieldSettings' => [
                'autoincrement' => 1
            ]
        ], JSON_UNESCAPED_UNICODE)),
    ],
    '11' => [
        'image' => 'assets/images/demo/example1.jpg',
    ],
    '12' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '13' => [
        'image' => 'assets/images/demo/example9.jpg',
        'price' => 1000,
    ],
    '14' => [
        'image' => 'assets/images/demo/example8.jpg',
        'price' => 1000,
    ],
    '15' => [
        'image' => 'assets/images/demo/example7.jpg',
        'price' => 1000,
    ],
    '16' => [
        'image' => 'assets/images/demo/example6.jpg',
        'price' => 1000,
    ],
    '17' => [
        'image' => 'assets/images/demo/example5.jpg',
        'price' => 1000,
    ],
    '26' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '27' => [
        'image' => 'assets/images/demo/example3.jpg',
    ],
];

$site_tmplvar_contentvalues = $modx->getFullTablename('site_tmplvar_contentvalues');

foreach ($tmplvar_contentvalues_data as $docid => $values) {
    foreach ($values as $tvname => $value) {
        $modx->db->insert([
            'tmplvarid' => $tvs[$tvname],
            'contentid' => $docid,
            'value'     => $value,
        ], $site_tmplvar_contentvalues);
    }
}

$pagebuilder_data = [
    [
        'document_id' => '2',
        'container' => 'main_cycle',
        'config' => 'main_cycle_slide',
        'values' => '{"image":"assets/images/demo/example2.jpg","mobile_image":"","text":"<p>Шаблон 4 для онлайн-магазинов одежды<br />и всяких модных вещей</p>","color":"","text_decoration":"","invert":{}}',
        'index' => '0',
    ], [
        'document_id' => '2',
        'container' => 'main_cycle',
        'config' => 'main_cycle_slide',
        'values' => '{"image":"assets/images/demo/example3.jpg","mobile_image":"","text":"<p>Шаблон 4 для онлайн-магазинов одежды<br />и всяких модных вещей</p>","color":"","text_decoration":"","invert":{"0":"1"}}',
        'index' => '1',
    ], [
        'document_id' => '19',
        'container' => 'faq',
        'config' => 'faq',
        'values' => '{"question":"Как заказать?","answer":"<p>Можете выбрать любой, удобный для Вас, вариант общения:</p>\\n<ul>\\n<li>Приезжайте к нам в офис и обсудим вашу мебель в уютной атмосфере нашего офиса за чашечкой кофе. Мы всегда рады видеть Вас!</li>\\n<li>Позвоните нам по телефону и закажите бесплатный выезд нашего специалиста к Вам.</li>\\n<li>Оставьте заявку на расчет мебели или на дизайн-проект по форме на сайте. Мы перезвоним Вам для уточнения деталей в ближайшие сроки.</li>\\n<li>Пришлите нам на эл. почту фото или эскизы мебели, которую нужно посчитать или прорисовать.</li>\\n</ul>\\n<p>Ждем Вас!</p>"}',
        'index' => '0',
    ], [
        'document_id' => '19',
        'container' => 'faq',
        'config' => 'faq',
        'values' => '{"question":"Ваш специалист может выехать на место и посоветовать что-нибудь?","answer":"<p>Да, конечно. Кроме того, он может взять с собой необходимые образцы цвета и материалов, чтобы подобрать оптимальное решение для Вас.</p>\\n<p>Выезд бесплатный, достаточно позвонить нам по телефону и договориться о времени встречи.</p>"}',
        'index' => '1',
    ], [
        'document_id' => '21',
        'container' => 'contacts',
        'config' => 'contacts',
        'values' => '{"title":"Пермь","text":"<p class=\\"font-Open_Sans1\\">[(client_company_address)], 2-ой этаж, оф. 212. Есть парковка.</p>\\n<p class=\\"font-Open_Sans1\\">тел: [(client_company_phone)]<br />e-mail:&nbsp;<span id=\\"cloak4fdf65ec13202238f55d6a1a06729e34\\"><a href=\\"mailto:[(client_company_email)]\\" data-goal=\\"form:email_click\\">[(client_company_email)]</a></span></p>","map_code":"<iframe style=\\"border: 0;\\" src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2113.6586441579157!2d56.254264516019404!3d58.010548081216925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43e8c6d72a8b776f%3A0xe3fb69ab6b7f51b5!2z0YPQuy4g0J_Rg9GI0LrQuNC90LAsIDksINCf0LXRgNC80YwsINCf0LXRgNC80YHQutC40Lkg0LrRgNCw0LksIDYxNDA0NQ!5e0!3m2!1sru!2sru!4v1564562063373!5m2!1sru!2sru\\" width=\\"1400\\" height=\\"600\\" frameborder=\\"0\\" allowfullscreen=\\"allowfullscreen\\"></iframe>"}',
        'index' => '0',
    ],
];

$pagebuilder = $modx->getFullTablename('pagebuilder');

foreach ($pagebuilder_data as $row) {
    $row['values'] = $modx->db->escape($row['values']);
    $modx->db->insert($row, $pagebuilder);
}

$sg_data = [
    [
        'sg_id' => '1',
        'sg_image' => 'assets/galleries/13/example1.jpg',
        'sg_title' => 'example1',
        'sg_properties' => '{"width":1600,"height":1000,"size":320259}',
        'sg_rid' => '13',
        'sg_index' => '0',
        'sg_createdon' => '2021-09-21 11:31:42',
    ], [
        'sg_id' => '2',
        'sg_image' => 'assets/galleries/13/example2.jpg',
        'sg_title' => 'example2',
        'sg_properties' => '{"width":1600,"height":1000,"size":166894}',
        'sg_rid' => '13',
        'sg_index' => '1',
        'sg_createdon' => '2021-09-21 11:31:42',
    ], [
        'sg_id' => '3',
        'sg_image' => 'assets/galleries/13/example3.jpg',
        'sg_title' => 'example3',
        'sg_properties' => '{"width":1600,"height":1000,"size":351088}',
        'sg_rid' => '13',
        'sg_index' => '2',
        'sg_createdon' => '2021-09-21 11:31:43',
    ], [
        'sg_id' => '4',
        'sg_image' => 'assets/galleries/13/example4.jpg',
        'sg_title' => 'example4',
        'sg_properties' => '{"width":1440,"height":900,"size":269155}',
        'sg_rid' => '13',
        'sg_index' => '3',
        'sg_createdon' => '2021-09-21 11:31:43',
    ],
];

include_once(MODX_BASE_PATH . 'assets/plugins/simplegallery/lib/plugin.class.php');
global $modx_lang_attribute;
$plugin = new \SimpleGallery\sgPlugin($modx, $modx_lang_attribute);
$plugin->createTable();

$sg_images = $modx->getFullTablename('sg_images');

$modx->db->delete($sg_images);

foreach ($sg_data as $row) {
    $modx->db->insert($row, $sg_images);
}

$site_plugins = $modx->getFullTablename('site_plugins');
$site_plugin_events = $modx->getFullTablename('site_plugin_events');

$modx->db->update(['disabled' => 0], $site_plugins, "`name` = 'evoSearch'");

$props = [
    'site_plugins' => [
        'SimpleGallery' => [
            'tabName' => 'Изображения',
            'templates' => $templates['product'],
        ],

        'LessCompiler' => [
            'path' => 'assets/templates/default/css/',
            'vars' => 'assets/templates/default/css/variables.json',
        ],

        'Commerce' => [
            'cart_page_id' => '29',
            'order_page_id' => '29',
            'product_templates' => $templates['product'],
        ],

        'evoSearch' => [
            'prepare' => 'savePageBuilderContent',
        ],
    ],

    'site_modules' => [
        'eLists' => [
            'param_tv_id' => $tvs['tovarparams'],
            'product_templates_id' => $templates['product'],
            'param_cat_id' => $modx->db->getValue($modx->db->select('id', $modx->getFullTablename('categories'), "`category` = 'Параметры'")),
            'tovarChunkName' => 'product_item',
        ],
    ],
];

foreach ($props as $table => $tableProps) {
    $table = $modx->getFullTablename($table);

    foreach ($tableProps as $name => $itemProps) {
        $item = $modx->db->getRow($modx->db->select('*', $table, "`name` = '$name'"));

        if ($item) {
            $properties = json_decode($item['properties'], true);

            if ($properties) {
                foreach ($itemProps as $key => $value) {
                    if (!empty($properties[$key][0])) {
                        $properties[$key][0]['value'] = (string)$value;
                    }
                }

                $modx->db->update(['properties' => json_encode($properties, JSON_UNESCAPED_UNICODE)], $table, "`id` = '" . $item['id'] . "'");
            }
        }
    }
}

$system_settings = $modx->getFullTablename('system_settings');

$settings = [
    'group_tvs'                   => 2,
    'global_tabs'                 => 1,
    'emailsender'                 => 'noreply@' . $_SERVER['HTTP_HOST'],
    'client_company_logo'         => '',
    'client_company_address'      => 'г. Пермь, ул. Уличная, 9',
    'client_company_phone'        => '+7 (999) 999-99-99',
    'client_company_email'        => 'mail@' . $_SERVER['HTTP_HOST'],
    'client_preview_alg'          => 'video',
    'client_social_facebook'      => '#',
    'client_social_vkontakte'     => '#',
    'client_social_instagram'     => '#',
    'client_social_youtube'       => '#',
    'client_social_odnoklassniki' => '#',
    'client_email_recipients'     => 'mnoskov@trylike.ru',
];

foreach ($settings as $key => $value) {
    $modx->db->save([
        'setting_name'  => $key,
        'setting_value' => $value,
    ], $system_settings, "`setting_name` = '$key'");
}


// remove installer
$query = $modx->db->select('id', $site_plugins, "`name` = 'Install'");

if ($id = $modx->db->getValue($query)) {
   $modx->db->delete($site_plugins, "`id` = '$id'");
   $modx->db->delete($site_plugin_events, "`pluginid` = '$id'");
};
