/**
 * list
 *
 * list
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as list_SQL_ID
 */
{{header}}

<div class="container">
	<h1 class="page-title">
		[*pagetitle*]
	</h1>
	
	{{breadcrumbs}}

	[[renderPaginated? 
		&tpl=`list_item`
		&ownerTPL=`list_wrap`
		&tvList=`image`
		&tvPrefix=``
		&orderBy=`menuindex ASC`
		&display=`4`
	]]
</div>

{{footer}}
