/**
 * reviews
 *
 * reviews
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as reviews_SQL_ID
 */
{{header}}

<div class="container">
	<h1 class="page-title">[*pagetitle*]</h1>

	<div class="user-content">
		[*content*]
	</div>
	
	[[DocLister?
		&parents=`[*id*]`
		&tvList=`image`
		&tpl=`reviews_item`
		&ownerTPL=`reviews_wrap`
		&orderBy=`menuindex DESC`
		&display=`10`
		&paginate=`pages`
		&TplNextP=`@CODE: ` 
		&TplPrevP=`@CODE: ` 
		&TplPage=`@CODE: <a href="[+link+]">[+num+]</a>` 
		&TplCurrentPage=`@CODE: <span class="current">[+num+]</span>` 
		&TplWrapPaginate=`@CODE: <div class="pagination">[+wrap+]</div>`
	]]

	<div class="text-xs-center">
		<p>Понравилась наша компания? Вы можете оставить отзыв, он будет опубликован после модерации.</p>
		<a href="#" class="btn btn-theme-hollow" data-toggle="modal" data-target="#review">Оставить отзыв</a>
	</div>
</div>

{{footer}}
