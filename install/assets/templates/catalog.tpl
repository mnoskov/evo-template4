/**
 * catalog
 *
 * catalog
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as catalog_SQL_ID
 */
{{header}}

<div class="container">
	<div class="btn btn-sm btn-theme-hollow pull-xs-right hidden-sm-up toggle-filters">Фильтры</div>

	<h1 class="page-title">
		[*pagetitle*]
	</h1>

	[[renderCategories]]
</div>

[!renderFilters!]

<div class="container">
	[!renderProducts!]
</div>

{{footer}}
