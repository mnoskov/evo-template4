/**
 * faq
 *
 * faq
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as faq_SQL_ID
 */
{{header}}

<div class="container">
	<h1 class="page-title">[*pagetitle*]</h1>

	[[PageBuilder? &container=`faq`]]
</div>

{{footer}}
