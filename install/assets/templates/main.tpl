/**
 * main
 *
 * main
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as main_SQL_ID
 */
{{header}}

[[PageBuilder? &container=`main_cycle`]]

{{footer}}
