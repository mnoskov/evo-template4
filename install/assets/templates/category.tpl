/**
 * category
 *
 * category
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as category_SQL_ID
 */
{{header}}

<div class="container">
	<h1 class="page-title">
		[*pagetitle*]
	</h1>
	
	{{breadcrumbs}}

	[[renderCategories]]

	[[renderProducts? 
		&depth=`4`
		&addWhereList=`c.template = '4'`
	]]
</div>

{{footer}}
