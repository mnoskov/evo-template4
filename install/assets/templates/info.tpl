/**
 * info
 *
 * info
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as info_SQL_ID
 */
{{header}}

<div class="container">
	<h1 class="page-title">
		[*pagetitle*]
	</h1>
	
	{{breadcrumbs}}

	<div class="user-content">
		[*content*]
	</div>
	
	[[PageBuilder? &container=`page`]]
</div>

{{footer}}
