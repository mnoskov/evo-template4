/**
 * search
 *
 * search
 *
 * @category	template
 * @internal	@modx_category
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as search_SQL_ID
 */
{{header}}

<div class="container">
    <h1 class="page-title">
        [*pagetitle*]
    </h1>

    {{breadcrumbs}}

    [!evoSearch?
        &id=`list`
        &tvPrefix=``
        &tvList=`image,price`
        &tpl=`product_item`
        &ownerTPL=`product_wrap`
        &orderBy=`pub_date DESC`
    !]
</div>

{{footer}}
