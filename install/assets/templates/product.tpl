/**
 * product
 *
 * product
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as product_SQL_ID
 */
{{header}}

<div class="container">
	<div class="hidden-md-up">
		<div class="page-title">[[getField? &fields=`menutitle,pagetitle`]]</div>
		{{breadcrumbs}}
	</div>
	
	<div class="product-columns">
		[[sgLister? &tpl=`product_images_slide` &ownerTPL=`product_images_cycle`]]

		<div class="product-info">
			<div class="hidden-sm-down">
				<h1 class="page-title">[[getField? &fields=`menutitle,pagetitle`]]</h1>
				{{breadcrumbs}}
			</div>

			<form data-commerce-action="add">
				[[CommerceOptions?
					&radioTpl=`product_option`
				]]
				
				<div class="price-row">
					<div data-commerce-price="[*price*]" class="price">
						[[PriceFormat? &price=`[*price*]`]]
					</div>

					<input type="hidden" name="id" value="[*id*]">
					<button type="submit" class="btn btn-theme">В корзину</button>
				</div>
			</form>
			
			<div class="accordeon">
				<div class="item active">
					<div class="title">
						<i class="tpl-icon-arrow-down-o"></i>
						Информация о товаре
					</div>
					
					<div class="hidden" style="display: block;">
						<div class="user-content">
							[*content*]
						</div>
					</div>
				</div>

				[[if? &is=`[*materials*]~~!empty` &separator=`~~` &then=`
					<div class="item active">
						<div class="title">
							<i class="tpl-icon-arrow-down-o"></i>
							Материалы
						</div>

						<div class="hidden" style="display: block;">
							<div class="user-content">
								[*materials*]
							</div>
						</div>
					</div>
				`]]
			</div>
		</div>
	</div>
	
	[[DocLister? 
		&parents=`[*parent*]` 
		&tpl=`product_item` 
		&ownerTPL=`related_product_wrap` 
		&tvList=`image,price` 
		&tvPrefix=``
		&addWhereList=`c.template = '4' AND c.id != '[*id*]'`
		&display=`3` 
		&noneWrapOuter=`0`
	]]
</div>

{{footer}}
