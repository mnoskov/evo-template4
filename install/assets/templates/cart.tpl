/**
 * cart
 *
 * cart
 *
 * @category	template
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@save_sql_id_as cart_SQL_ID
 */
{{header}}

<div class="container">
	<div class="row">
		<div class="col-md-7">
			[!Cart?
				&tvList=`image`
				&ownerTPL=`cart_wrap`
				&tpl=`cart_row`
				&defaultOptionsRender=`0`
				&prepare=`prepareRenderCartOptions`
			!]
		</div>

		<div class="col-md-5">
			[!Order?
				&formTpl=`order_form`
				&deliveryTpl=`@CODE:[+wrap+]`
				&paymentsTpl=`@CODE:[+wrap+]`
			!]
		</div>
	</div>
</div>

{{footer}}
