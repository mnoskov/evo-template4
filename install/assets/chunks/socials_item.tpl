/**
 * socials_item
 *
 * socials_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<a href="[+url+]" target="_blank" rel="nofollow" title="[+title+]" class="tpl-icon-social-[+code+]-o"></a>
