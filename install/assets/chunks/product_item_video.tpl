/**
 * product_item_video
 *
 * product_item_video
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="item-preview preview-video" data-video="[[makeEmbed? &in=`[+video+]`]]"></div>
