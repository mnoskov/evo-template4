/**
 * list_item
 *
 * list_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<a class="list-item" href="[+url+]">
	<div class="item-image" style="background-image: url('[[phpthumb? &input=`[+image+]` &options=`w=860h=570,zc=1,f=jpg,bg=FFFFFF`]]');"></div>
	
	<div class="item-block">
		<div class="item-title">[+title+]</div>
		<div class="user-content">[+introtext+]</div>
		<div class="btn btn-theme-hollow">Смотреть</div>
	</div>
</a>
