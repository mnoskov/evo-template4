/**
 * product_item
 *
 * product_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<li[+item_class+]>
	<div class="product-item">
		<div class="image">
			<span style="background-image: url('[[phpthumb? &input=`[+image+]` &options=`w=319,h=400,zc=1,f=jpg`]]');"></span>
			[+hover+]
		</div>

		<form data-commerce-action="add">
			[[CommerceOptions?
				&docid=`[+id+]`
				&registerScripts=`1`
				&tvTpl=`product_static_options_group`
				&radioTpl=`product_static_option`
			]]
			
			[+pagetitle+]
			
			<div data-commerce-price="[+price+]">
				[[PriceFormat? &price=`[+price+]`]]
			</div>
		</form>
		
		<a href="[+url+]"></a>
	</div>
