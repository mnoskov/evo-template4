/**
 * footer
 *
 * footer
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
	</div>

	<div class="footer">
		<div class="container">
			<div class="pull-md-right">
				{{phone}}
				[[renderSocials? &tpl=`socials_item`]]
			</div>

			{{menu}}
			
			<div class="policy">
				<a href="[(client_policy)]" target="_blank">Политика конфиденциальности</a>
			</div>
		</div>
	</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="response">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close icon-close" data-dismiss="modal"></button>
				<div class="modal-body">
					<div class="response"></div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.9.3/toastify.min.js"></script>
	<script src="/assets/templates/default/js/jquery.maskedinput.min.js"></script>
	<script src="/assets/templates/default/js/common.js"></script>
	<script src="/assets/templates/default/js/template.js"></script>
	[(client_body_end_scripts)]
</body>
</html>

