/**
 * head_cart
 *
 * head_cart
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="minicart" data-commerce-cart="[+hash+]">
	<a href="[~[+settings.cart_page_id+]~]" title="[%cart.to_cart%]">
		<span class="tpl-icon-cart"></span>
		<span class="count">[+count+]</span>
	</a>
</div>

