/**
 * reviews_item
 *
 * reviews_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="review-item">
	<div class="title">
		[+pagetitle+]
	</div>

	<div class="text-muted">
		[+introtext+]
	</div>

	<div class="user-content">
		[+content+]
		[[smartCrop? &str=`[+content+]` &length=`250`]]
	</div>
</div>

