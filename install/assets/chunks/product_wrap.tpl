/**
 * product_wrap
 *
 * product_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="products-list" id="eFiltr_results">
	<ul class="items">
		[+dl.wrap+]
	</ul>
	
	[+pages+]
</div>
