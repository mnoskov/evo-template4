/**
 * cart_row_option
 *
 * cart_row_option
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div>
	[+key+]:<br>
	[+option+]
</div>
