/**
 * phone
 *
 * phone
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="phone-block">
	<span class="tapable-phone" data-goal="form:phone_click">[(client_company_phone)]</span><br>
	<a href="#" data-toggle="modal" data-target="#callback" class="callback">Обратный звонок</a>
</div>
