/**
 * list_wrap
 *
 * list_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="list">
	[+dl.wrap+]	
	[+pages+]
</div>
