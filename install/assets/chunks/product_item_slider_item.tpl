/**
 * product_item_slider_item
 *
 * product_item_slider_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="slide" style="background-image: url([[phpthumb? &input=`[+sg_image+]` &options=`w=319,h=400,zc=1,f=jpg`]]);"></div>

