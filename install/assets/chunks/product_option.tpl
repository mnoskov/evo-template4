/**
 * product_option
 *
 * product_option
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<li data-tvco-row[+hidden_style+]>
    <label>
        <input type="radio" name="[+tv.controlname+]" value="[+value.id+]" data-value="[+value.value_id+]"[+selected_attr+]>
        [!if? &is=`[+value.image+]~~!empty` &separator=`~~` &then=`
			<img src="[[phpthumb? &input=`[+value.image+]]]" alt="[+value.title+]" title="[+value.title+][+modifier+]">
		` &else=`
			<span title="[+value.title+][+modifier+]">[+value.title+]</span>
		`!]
    </label>

