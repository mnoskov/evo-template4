/**
 * category_wrap
 *
 * category_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="categories-list">
	<ul class="items">
		[+dl.wrap+]
	</ul>
</div>
