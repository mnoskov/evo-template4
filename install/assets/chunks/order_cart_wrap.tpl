/**
 * order_cart_wrap
 *
 * order_cart_wrap
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div data-commerce-cart="[+hash+]">
    <div class="table-responsive">
        <table class="table">
			[+subtotals+]

			<tr>
				<td>[%cart.total%]:</td>
				<td class="text-xs-right">[[PriceFormat? &price=`[+total+]` &convert=`0`]]</td>
			</tr>
        </table>
    </div>
</div>

