/**
 * modal_order
 *
 * template of simple order modal
 *
 * @category	chunk
 * @internal	@modx_category Формы
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="modal fade" tabindex="-1" role="dialog" id="order">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="#" data-goal="form:zakaz" class="ajax">
				<button type="button" class="close tpl-icon-close" data-dismiss="modal"></button>
				<div class="modal-title">Узнать подробнее</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" name="name" class="form-control" placeholder="Ваше имя *">
					</div>

					<div class="form-group">
						<input type="text" name="phone" class="mask-phone form-control" placeholder="Контактный телефон *">
					</div>
					
					<div class="form-group">
						<input type="text" name="email" class="form-control" placeholder="Email *">
					</div>
					
					<div class="form-group file-upload">
						<label>
							<span class="wi-group">
								<i class="tpl-icon-attach"></i>
								<input type="file" name="file" onchange="this.nextElementSibling.innerHTML = this.value.replace(/^.*[\\/]/, '')">
								<span class="filename">Прикрепить файл</span>
							</span>
						</label>
					</div>
					
					<div class="text-xs-center">
						<input type="hidden" name="pid" value="[*id*]">
						<input type="hidden" name="formid" value="order">
						<button type="submit" class="btn btn-theme">Отправить</button>
					</div>

					{{policy_note}}
				</div>
			</form>
		</div>
	</div>
</div>

