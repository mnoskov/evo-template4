/**
 * product_static_option
 *
 * product_static_option
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<li data-tvco-row[+hidden_style+]>
	<input type="hidden" name="[+tv.controlname+]" value="[+value.id+]" data-value="[+value.value_id+]"[+selected_attr+]>
    [[if? &is=`[+value.image+]~~!empty` &separator=`~~` &then=`
		<img src="[[phpthumb? &input=`[+value.image+]]]" alt="[+value.title+]" title="[+value.title+]">
	` &else=`
		<span>[+value.title+]</span>
	`]]

