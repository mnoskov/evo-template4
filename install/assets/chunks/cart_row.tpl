/**
 * cart_row
 *
 * cart_row
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="cart-item" data-id="[+id+]" data-commerce-row="[+row+]">
	<span data-commerce-action="remove" title="[%cart.remove%]" class="tpl-icon-close"></span>
	
    <a href="[+url+]">
        <img src="[[phpthumb? &input=`[+tv.image+]` &options=`w=80`]]" class="img-fluid" alt="[+e.title+]">
	</a>																												  

    <div class="item-info">
        <div class="title">[+name+]</div>
		
		<div class="item-price">
			[[PriceFormat? &price=`[+total+]` &convert=`0`]]
		</div>

        <div class="item-chars">
            [+options+]
			
			<div class="item-count">
				Кол-во:
				<div class="count-input">
					<input type="text" name="count" class="form-control" value="[+count+]" data-commerce-action="recount">
					<button type="button" data-commerce-action="decrease" title="[%cart.decrease%]" class="tpl-icon-subtract"></button>
					<button type="button" data-commerce-action="increase" title="[%cart.increase%]" class="tpl-icon-add"></button>
				</div>
			</div>
        </div>
	</div>
</div>

