/**
 * product_images_slide
 *
 * product_images_slide
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="slide">
	<a href="[[phpthumb? &input=`[+sg_image+]` &options=`w=1600,h=1600,f=jpg`]]" class="image" data-fancybox="gallery">
		<img src="[[phpthumb? &input=`[+sg_image+]` &options=`w=665,f=jpg`]]">
	</a>
</div>
