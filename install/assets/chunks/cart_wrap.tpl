/**
 * cart_wrap
 *
 * cart_wrap
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div data-commerce-cart="[+hash+]" class="panel">
	<div class="panel-header">
		Корзина
	</div>
	
	<div class="panel-body">
		[+dl.wrap+]

		<p class="cart-bottom">
			<b>[%cart.total%]: [[PriceFormat? &price=`[+items_price+]` &convert=`0`]]</b>
			<span class="btn btn-theme-hollow" data-commerce-action="clean">[%cart.clean%]</span>
		</p>
	</div>
</div>

