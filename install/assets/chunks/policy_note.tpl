/**
 * policy_note
 *
 * policy note
 *
 * @category	chunk
 * @internal	@modx_category Формы
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="policy-note">
    <div class="form-group">
        <label>
            <span class="checkbox-wrap">
                <input type="checkbox" name="agree" value="1" checked>
                <span class="checkbox"></span>
            </span>

            Я согласен на <a href="[(client_policy)]" target="_blank">обработку персональных данных</a>
        </label>
    </div>
</div>

