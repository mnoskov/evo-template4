/**
 * head_cart_empty
 *
 * head_cart_empty
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="minicart" data-commerce-cart="[+hash+]">
	<span class="tpl-icon-cart"></a>
</div>

