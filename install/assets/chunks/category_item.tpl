/**
 * category_item
 *
 * category_item
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<li class="item">
	<a href="[+url+]">
		<span class="image" style="background-image: url([[phpthumb? &input=`[+image+]` &options=`w=183,h=183,zc=1,f=jpg`]]);" title="[+e.title+]"></span>
		<span class="title">[+title+]</span>
	</a>
