/**
 * product_item_slider_wrap
 *
 * product_item_slider_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="item-preview preview-slider slick" data-slick='{"dots": true, "arrows": false, "speed": 0}'>
    [+dl.wrap+]
</div>

