/**
 * product_static_options_group
 *
 * product_static_options_group
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="tvco-var tvco-[+tv.output_type+]" data-tvco-block="[+hash+]" data-id="[+tv.id+]">
    <ul>[+wrap+]</ul>
</div>
