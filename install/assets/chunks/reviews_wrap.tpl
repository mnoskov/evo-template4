/**
 * reviews_wrap
 *
 * reviews_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="reviews-list">
    [+dl.wrap+]
    [+pages+]
</div>

