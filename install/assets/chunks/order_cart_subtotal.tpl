/**
 * order_cart_subtotal
 *
 * order_cart_subtotal
 *
 * @category	chunk
 * @internal	@modx_category Commerce
 * @internal	@installset base
 * @internal	@overwrite true
 */
<tr>
    <td>[+title+]:</td>
    <td class="text-xs-right">[[PriceFormat? &price=`[+price+]` &convert=`0`]]</td>
</tr>

