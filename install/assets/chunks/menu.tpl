/**
 * menu
 *
 * menu
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="menu">
	[[DLMenu? &parents=`1` &maxDepth=`1` &addWhereList=`c.id != 2`]]
</div>
