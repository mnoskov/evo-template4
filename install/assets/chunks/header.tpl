/**
 * header
 *
 * header
 *
 * @category	chunk
 * @internal	@modx_category
 * @internal	@installset base
 * @internal	@overwrite true
 */
<!DOCTYPE html>
<html lang="ru">
<head>
	<title>[[metatitle]]</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<base href="/">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	[[metaheaders]]
	<link rel="icon" type="image/x-icon" href="favicon.ico">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.9.3/toastify.min.css">
	<link rel="stylesheet" href="assets/templates/default/css/style.css">
	<link rel="stylesheet" href="assets/templates/default/css/template.css">
	[(client_head_scripts)]
</head>
<body class="[[template]]">
	[(client_body_start_scripts)]
	<div class="wrap">
		<div class="fixed-overlay toggle-menu"></div>

		<div class="fixed-menu">
			<div class="search-form-wrap">
				<form action="[~28~]" method="get">
					<div class="search-form">
						<input type="text" name="search" class="form-control" placeholder="Найти"><button type="submit"><i class="tpl-icon-search"></i></button>
					</div>
				</form>
			</div>

			<div class="container-fluid">
				{{menu}}
			</div>

			<div class="blocks-container">
				<div class="text-xs-left">
					{{phone}}
				</div>

				<div class="text-xs-right">
					[[renderSocials? &tpl=`socials_item`]]
				</div>
			</div>
		</div>

		<div class="head-placeholder">
			<div class="head">
				<div class="container[[if? &is=`[*id*]:not:2` &then=`-fluid`]]">
					<div class="left-column">
						<span class="toggle-menu">
							<i class="tpl-icon-menu"></i>
							<i class="tpl-icon-close"></i>
						</span>

						{{phone}}
					</div>

					<div class="right-column">
						<span class="toggle-search hidden-xs-down">
							<i class="tpl-icon-search"></i>
						</span>

						[!Cart? &theme=`mini` &ownerTPL=`head_cart` &noneTPL=`head_cart_empty`!]
					</div>

					<div class="logo">
						<a href="/">
							[[if? &is=`[(client_logo)]:!empty` &then=`
								<img src="[(client_logo)]" alt="[(site_name)]" class="colored">
								<img src="[(client_logo_inverted)]" alt="[(site_name)]" class="white">
							` &else=`
								{{logo}}
							`]]
						</a>
					</div>
				</div>
			</div>
		</div>
