/**
 * related_product_wrap
 *
 * related_product_wrap
 *
 * @category	chunk
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 */
<div class="row">
	<div class="col-xl-10 offset-xl-1">
		<div class="related-title">Вам могут понравиться</div>
	</div>
</div>

<div class="products-list">
	<ul class="items">
		[+dl.wrap+]
	</ul>
</div>
