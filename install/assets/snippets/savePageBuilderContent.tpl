//<?php
/**
 * savePageBuilderContent
 *
 * savePageBuilderContent
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */


$data['content'] .= strip_tags($modx->runSnippet('PageBuilder', [
	'docid'     => $data['id'],
	'container' => 'page',
]));

return $data;
