//<?php
/**
 * renderCategories
 *
 * renderCategories
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */


$params = array_merge([
	'tpl'           => 'category_item',
	'ownerTPL'      => 'category_wrap',
	'tvList'        => 'image',
	'tvPrefix'      => '',
	'orderBy'       => 'menuindex',
	'addWhereList'  => "c.template = '" . $modx->templateConstants['category_template_id'] . "'",
	'noneWrapOuter' => '0',
], $params);

return $modx->runSnippet('DocLister', $params);
