//<?php
/**
 * renderProducts
 *
 * renderProducts
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */

$params = array_merge([
	'snippet'  => 'eFilterResult',
	'parents'  => $data['id'],
	'tpl'      => 'product_item',
	'ownerTPL' => 'product_wrap',
	'tvList'   => 'image,price,video,preview_alg',
	'tvPrefix' => '',
	'orderBy'  => 'menuindex ASC',
	'display'  => 16,
], $params);

if (isset($params['prepare'])) {
	$params['prepare'] = [ $params['prepare'] ];
} else {
	$params['prepare'] = [];
}

$params['prepare'][] = function($data, $modx, $_DL, $_eDL) {
	$data['hover'] = '';
	
	$alg = !empty($data['preview_alg']) && $data['preview_alg'] != 'inherit' ? $data['preview_alg'] : $modx->getConfig('client_preview_alg');
	
	switch ($alg) {
		case 'slider': {
			$data['hover'] = $modx->runSnippet('sgLister', [
				'parents'  => $data['id'],
				'tpl'      => 'product_item_slider_item',
				'ownerTPL' => 'product_item_slider_wrap',
				'tvPrefix' => '',
				'display'  => 5,
			]);
			break;
		}

		case 'video': {
			if (!empty($data['video'])) {
				$data['hover'] = $_DL->parseChunk('product_item_video', $data);
			}

			break;
		}
	}
	
	$total = $_DL->getChildrenCount();

	if ($data['iteration'] % 7 == 1 || $data['dl.is_last'] || ($data['iteration'] + 1 == $data['dl.display'] && ($data['iteration'] % 7) % 2 == 0)) {
		$data['item_class'] = ' class="big"';
	}
	
	return $data;
};

return $modx->runSnippet('renderPaginated', $params);
