//<?php
/**
 * prepareRenderCartOptions
 *
 * prepareRenderCartOptions
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */

$data['options'] = '';

if (!empty($data['meta']['tvco'])) {
	foreach ($data['meta']['tvco'] as $option) {
		$data['options'] .= '<div>' . $option['tv']['caption'] . ':<div class="value">' . (!empty($option['image']) ? '<img src="' . $modx->runSnippet('phpthumb', ['input' => $option['image'], 'options' => 'w=32,h=32,zc=1']) . '" alt="' . $option['value'] . '">' : $option['value']) . '</div></div>';
	}
}
	
return $data;
