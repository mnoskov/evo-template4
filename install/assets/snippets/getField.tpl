//<?php
/**
 * getField
 *
 * getField
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */

	
foreach (explode(',', $fields) as $field) {
	if (empty($modx->documentObject[$field])) {
		continue;
	}
	
	if (is_array($modx->documentObject[$field])) {
		if (!empty($modx->documentObject[$field][1])) {
			continue;
		}
		
		return $modx->documentObject[$field][1];
	}
	
	return $modx->documentObject[$field];
}
