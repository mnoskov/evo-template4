//<?php
/**
 * renderFilters
 *
 * renderFilters
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */

$modx->runSnippet('eFilter', [
    'cfg'   => 'template4',
    'depth' => 0,
    'ajax'  => 1,
]);

$form = $modx->getPlaceholder('eFilter_form');

if (!empty($form)) {
	return '
	<div class="fixed-overlay toggle-filters"></div>
	<div class="filters">
		<div class="toggle-filters hidden-xs-down">Фильтры<i class="tpl-icon-arrow-down"></i></div>
		<div class="filters-form">
			<div class="container">
				<div class="toggle-filters hidden-sm-up tpl-icon-close"></div>
				' . $form . '
			</div>
		</div>
	</div>';
}
