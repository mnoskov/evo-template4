//<?php
/**
 * renderPaginated
 *
 * renderPaginated
 *
 * @category	snippet
 * @internal	@modx_category 
 * @internal	@installset base
 * @internal	@overwrite true
 * @internal	@properties 
 */

$params = array_merge([
	'contentPlaceholder' => 1,
	'paginate'           => 'pages',
	'PrevNextAlwaysShow' => 1,
	'TplNextP'           => '@CODE: <a href="[+link+]" class="btn btn-theme-hollow">Следующая</a>',
	'TplPrevP'           => '@CODE: <a href="[+link+]" class="btn btn-theme-hollow">Предыдущая</a>',
	'TplNextI'           => '@CODE: <span class="btn btn-theme-hollow">Следующая</span>',
	'TplPrevI'           => '@CODE: <span class="btn btn-theme-hollow">Предыдущая</span>',
	'TplPage'            => '@CODE:',
	'TplCurrentPage'     => '@CODE: <span class="current">Страница [+num+] из [+totalPages+]</span>',
	'TplWrapPaginate'    => '@CODE: <div class="pagination">[+wrap+]</div>',
], $params);

$modx->runSnippet($params['snippet'] ?? 'DocLister', $params);

return $modx->getPlaceholder('contentPlaceholder');
