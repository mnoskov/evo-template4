<?php

if (!defined('IN_PARSER_MODE')) {
    return [];
}

return [
    'rules' => [
        'msg' => [
            'required' => 'Введите отзыв',
        ],
        'name' => [
            'required' => 'Введите имя',
            'lengthBetween' => [
                'params'  => [2, 100],
                'message' => 'Имя должно быть от 2 до 100 символов',
            ]
        ],
        'phone' => [
            'required' => 'Введите номер телефона',
            'matches' => [
                'params'  => '/^\+?[78]\s?\(\d{3}\)\s?\d{3}-\d\d-\d\d$/',
                'message' => 'Формат телефона неверный',
            ]
        ],
        'agree' => [
            'required' => 'Отметьте согласие',
        ],
    ],
    'successMessage' => 'Ваш отзыв отправлен! Он будет опубликован после модерации.',
    'subject' => 'Отзыв!',
    'reportTpl' => '@CODE:
        <b>Отзыв!</b>
        <table>
            <tr><td>Имя:&nbsp;</td><td>[+name.value+]</td></tr>
            <tr><td>Телефон:&nbsp;</td><td>[+phone.value+]</td></tr>
            <tr><td>Текст отзыва:&nbsp;</td><td>[+msg.value+]</td></tr>
        </table>
    ',
];
