<?php

if (!defined('IN_PARSER_MODE')) {
    return [];
}

return [
    'rules' => [
        'name' => [
            'required' => 'Введите имя',
            'lengthBetween' => [
                'params'  => [2, 100],
                'message' => 'Имя должно быть от 2 до 100 символов',
            ]
        ],
        'phone' => [
            'required' => 'Введите номер телефона',
            'matches' => [
                'params'  => '/^\+?[78]\s?\(\d{3}\)\s?\d{3}-\d\d-\d\d$/',
                'message' => 'Формат телефона неверный',
            ]
        ],
        'email' => [
            'required' => 'Введите email',
            'email'    => 'Формат адреса неверный',
        ],
        'agree' => [
            'required' => 'Отметьте согласие',
        ],
    ],
    'subject' => 'Заказ!',
    'successTpl' => '@CODE:<div class="title">Ваш заказ сформирован!</div><p>В ближайшее время наш менеджер свяжется с вами</p>',
    //'reportTpl' => 'order_report',
    //'ccSenderTpl' => 'order_reportback',
];
