<?php

include __DIR__ . '/config.default.php';

if(!defined('MODX_BASE_PATH')){die('What are you doing? Get out of here!');}

//общая форма фильтра
$tplFilterForm = '<form id="eFiltr" action="[+url+]" method="get">[+wrapper+]</form>';

//кнопка "сброса" фильтра
$tplFilterReset = '<div class="filter-side-buttons text-xs-right"><a href="[+reset_url+]" class="btn btn-sm btn-theme-hollow">Сбросить фильтр</a></div>';

//название категории фильтра
$filterCatName = '<div class="filter-group">[+cat_name+]</div>';

//диапазон
$tplRowInterval = '<div class="filter-option-body"><div class="labels"><div>от</div><div>до</div></div><input type="text" name="f[[+tv_id+]][min]" value="[+minval+]" data-min-val="[+minvalcurr+]" placeholder="[+minvalcurr+]"><input type="text" name="f[[+tv_id+]][max]" value="[+maxval+]" data-max-val="[+maxvalcurr+]" placeholder="[+maxvalcurr+]"></div>';
$tplOuterInterval = '
	<div class="filter-option fltr_block_interval fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_interval fltr_name[+tv_id+]">[+name+]</div>
		[+wrapper+]
	</div>
';

//чекбоксы
$tplRowCheckbox = '
	<label class="[+disabled+]">
		<input type="checkbox" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]><span>[+name+]</span>
	</label>
';
$tplOuterCheckbox = '
	<div class="filter-option fltr_block_checkbox fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_checkbox fltr_name[+tv_id+]">[+name+]</div>
		<div class="checks">
			[+wrapper+]
		</div>
	</div>
';

//радио - radio
$tplRowRadio = '
	<label class="[+disabled+]">
		<input type="radio" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]><span>[+name+]</span>
	</label>';
$tplOuterRadio = '
	<div class="filter-option fltr_block_radio fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_radio fltr_name[+tv_id+]">[+name+]</div>
		<div class="checks">
			[+wrapper+]
		</div>
	</div>
';

