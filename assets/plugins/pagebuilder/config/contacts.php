<?php

return [
    'container' => 'contacts',

    'templates' => [
        'owner' => '
            <div class="item">
                <div class="title">
                    [+title+]
                </div>

                <div class="user-content">
                    [+text+]
                </div>

                <a href="#" class="btn btn-theme">Показать карту</a>
                <div class="map" data-code="[+e_code+]"></div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Название',
            'type'    => 'text',
        ],

        'text' => [
            'caption' => 'Текст',
            'type'    => 'richtext',
            'options' => [
                'height' => '150px',
            ],
        ],

        'map_code' => [
            'caption' => 'Код карты',
            'type'    => 'textarea',
            'height'  => '100px',
        ],
    ],

    'prepare' => function($options, &$values) {
        $values['e_code'] = htmlentities($values['map_code']);
    },
];
