<?php

return [
    'title' => 'Слайдер',

    'show_in_templates' => $this->modx->templateConstants['main_template_id'],

    'placement' => 'tab',

    'templates' => [
        'owner' => '
            <div class="fullscreen-slider">
                <div class="slick" data-slick=\'{"arrows": false, "autoplay": true, "fade": true, "speed": 2000, "autoplaySpeed": 5000, "pauseOnHover": false, "pauseOnFocus": false}\'>
                    [+wrap+]
                </div>
            </div>
        ',
    ],
];
