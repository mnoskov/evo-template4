<?php

return [
    'placement' => 'content',

    'show_in_templates' => $this->modx->templateConstants['info_template_id'],

    'templates' => [
        'owner' => '[+wrap+]',
    ],
];
