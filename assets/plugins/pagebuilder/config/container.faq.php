<?php

return [
    'title' => 'FAQ',

    'show_in_templates' => $this->modx->templateConstants['faq_template_id'],

    'placement' => 'content',

    'templates' => [
        'owner' => '
            <div class="faq-items">
                [+wrap+]
            </div>
        ',
    ],
];
