<?php

return [
    'container' => 'faq',

    'templates' => [
        'owner' => '
            <div class="item">
                <div class="question toggle">
                    <i class="tpl-icon-arrow-down-o"></i>
                    <div class="title">Вопрос</div>

                    <div class="text">
                        [+question+]
                    </div>
                </div>

                <div class="answer" style="display: none;">
                    <div class="text">
                        [+answer+]
                    </div>
                </div>
            </div>
        ',
    ],

    'fields' => [
        'question' => [
            'caption' => 'Вопрос',
            'type'    => 'text',
        ],

        'answer' => [
            'caption' => 'Ответ',
            'type'    => 'richtext',
            'options' => [
                'height' => '200px',
            ],
        ],
    ],
];
