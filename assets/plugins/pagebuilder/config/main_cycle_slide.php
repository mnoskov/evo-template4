<?php

return [
    'title' => 'Слайд',

    'container' => 'main_cycle',

    'templates' => [
        'owner' => '
            <div class="slide" style="background-color: [[getImageColor? &source=`[+background+]`]];" data-invert="[+invert+]">
                <div class="slide-background" id="mainslide[+iteration+]"></div>

                <style>
                    #mainslide[+iteration+] { background-image: url(\'[[phpthumb? &input=`[[if? &is=`[+mobile_image+]~~!empty` &separator=`~~` &then=`[+mobile_image+]` &else=`[+image+]`]]` &options=`h=800`]]\'); }
                    @media (min-width: 576px) and (max-width: 991px) { #mainslide[+iteration+] { background-image: url(\'[[phpthumb? &input=`[+image+]` &options=`w=1200`]]\'); } }
                    @media (min-width: 992px) { #mainslide[+iteration+] { background-image: url(\'[[phpthumb? &input=`[+image+]` &options=`w=1920`]]\'); } }
                </style>

                <div class="slide-inner">
                    <div class="text" style="[[if? &is=`[+color+]:!empty` &then=` color: [+color+];`]]">
                        [[if? &is=`[+text_decoration+]:is:bg` &then=`
                            <span class="inline-padding"><span><span>[+text+]</span></span></span>
                        ` &else=`
                            <span class="[+text_decoration+]">[+text+]</span>
                        `]]
                    </div>
                </div>
            </div>
        ',
    ],

    'fields' => [
        'image' => [
            'caption' => 'Изображение',
            'type'    => 'image',
        ],

        'mobile_image' => [
            'caption' => 'Изображение на мобильном',
            'type'    => 'image',
        ],

        'text' => [
            'caption' => 'Текст',
            'type'    => 'richtext',
            'options' => [
                'height' => '100px',
            ],
        ],

        'color' => [
            'caption' => 'Цвет текста',
            'type'    => 'color',
        ],

        'text_decoration' => [
            'caption'  => 'Оформление текста',
            'type'     => 'radio',
            'elements' => ['' => 'Нет', 'bg' => 'Подложка', 'glowing' => 'Свечение', 'shadow' => 'Тень'],
            'layout'   => 'horizontal',
        ],

        'invert' => [
            'caption' => 'Инвертировать шапку',
            'type'    => 'checkbox',
            'elements' => [1 => 'Да'],
        ],
    ],
];
