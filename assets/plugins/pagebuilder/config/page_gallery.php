<?php

return [
    'title' => 'Галерея',

    'container' => 'page',

    'templates' => [
        'owner' => '
            <div class="page-gallery [+format+]">
                <ul class="items">
                    [+images+]
                </ul>
            </div>
        ',

        'images' => '
            <li>
                <a href="[+image+]" data-fancybox="gallery[+iteration+]">
                    <img src="[[phpthumb? &input=`[+image+]` &options=`[+size+],f=jpg`]]" class="img-fluid">
                </a>
        ',
    ],

    'fields' => [
        'format' => [
            'caption' => 'Формат вывода',
            'type' => 'dropdown',
            'elements' => [
                'default-1'   => 'Без обрезки, 1 в строке',
                'default-2'   => 'Без обрезки, 2 в строке',
                'default-3'   => 'Без обрезки, 3 в строке',
                'landscape-1' => 'Альбомный, 1 в строке',
                'landscape-2' => 'Альбомный, 2 в строке',
                'portrait-2'  => 'Книжный, 2 в строке',
                'portrait-3'  => 'Книжный, 3 в строке',
            ],
        ],

        'images' => [
            'caption' => 'Изображения',
            'type'    => 'group',
            'layout'  => 'gallery',
            'fields'  => [
                'image' => [
                    'caption' => 'Изображение',
                    'type'    => 'image',
                ],
            ],
        ],
    ],

    'prepare' => function($options, &$values) {
        $format = [
            'default-1'   => 'w=1360',
            'default-2'   => 'w=675',
            'default-3'   => 'w=446',
            'landscape-1' => 'w=1360,h=840,zc=1',
            'landscape-2' => 'w=675,h=418,zc=1',
            'portrait-2'  => 'w=675,h=1080,zc=1',
            'portrait-3'  => 'w=446,h=714,zc=1',
        ];

        foreach ($values['images'] as &$image) {
            $image['size'] = $format[ $values['format'] ];
        }

        unset($image);
    },
];
