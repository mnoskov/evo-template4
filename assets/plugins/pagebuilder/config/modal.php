<?php

return [
    'title' => 'Модальное окно',

    'container' => 'modals',

    'templates' => [
        'owner' => '
            <div class="modal fade custom-modal[+hasimage+]" tabindex="-1" role="dialog" id="[+alias+]">
                <div class="modal-dialog">
                    <div class="modal-content">
                        [+image_markup+]<div class="modal-form">
                            <form method="post" action="#" class="ajax" data-goal="[+goal+]">
                                <button type="button" class="close icon-close" data-dismiss="modal"></button>

                                <div class="modal-title">
                                    [+title+]
                                    [+subtitle+]
                                </div>

                                <div class="modal-body">
                                    [+fields+]

                                    {{policy_note}}

                                    <input type="hidden" name="pid" value="[*id*]">
                                    <input type="hidden" name="formid" value="custom">
                                    <input type="hidden" name="customid" value="[+iteration+]">
                                    <button type="submit" class="btn btn-theme">Отправить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            [+script+]
        ',

        'fields' => '
            <div class="form-group wi float-label">
                <label>
                    <span>[+caption+][+required_markup+]</span>
                </label>

                [+field_markup+]
                <i class="icon-[+icon+]"></i>
            </div>
        ',

        'image_markup' => '
            <div class="modal-image">
                <div class="image" style="background-image: url(\'[+image+]\');">
                    <div class="image-text" style="color: [+image_text_color+];">
                        <div class="user-content">
                            [+image_text+]
                        </div>
                    </div>
                </div>
            </div>
        ',

        'fields_markup' => [
            'text'     => '<input type="text" name="field_[+index+]" class="form-control">',
            'email'    => '<input type="text" name="email" class="form-control">',
            'phone'    => '<input type="text" name="phone" class="mask-phone form-control">',
            'textarea' => '<textarea name="field_[+index+]" class="form-control"></textarea>',
        ],

        'script_markup' => '
            <script>
                $(function() {
                    setTimeout(function() {
                        $("#[+alias+]").modal();
                    }, [+delay+]);
                });
            </script>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок формы',
            'type'    => 'text',
        ],

        'subtitle' => [
            'caption' => 'Подзаголовок',
            'type'    => 'text',
        ],

        'alias' => [
            'caption' => 'Идентификатор',
            'type'    => 'text',
            'note'    => 'Только символы латинского алфавита, без пробелов.',
        ],

        'fields' => [
            'caption'  => 'Поля',
            'type'     => 'group',
            'layout'   => 'horizontal',
            'fields'   => [
                'caption' => [
                    'caption' => 'Название поля',
                    'type'    => 'text',
                ],

                'type' => [
                    'caption'  => 'Тип поля',
                    'type'     => 'dropdown',
                    'elements' => [
                        'text'     => 'Текст',
                        'email'    => 'Email',
                        'phone'    => 'Телефон',
                        'textarea' => 'Многострочный текст',
                    ],
                ],

                'icon' => [
                    'caption' => 'Иконка',
                    'type'    => 'text',
                ],

                'required' => [
                    'caption'  => 'Обязательное?',
                    'type'     => 'checkbox',
                    'elements' => [1 => 'Да'],
                    'default'  => 0,
                ],
            ],
        ],

        'btn' => [
            'caption' => 'Текст на кнопке',
            'type'    => 'text',
            'default' => 'Отправить',
        ],

        'goal' => [
            'caption' => 'Имя цели',
            'type'    => 'text',
            'note'    => '&lt;category>:&lt;goal>',
        ],

        'show_image' => [
            'caption'  => 'Показывать изображение?',
            'type'     => 'checkbox',
            'elements' => [1 => 'Да'],
            'default'  => 0,
        ],

        'image' => [
            'caption' => 'Изображение',
            'type'    => 'image',
        ],

        'image_text' => [
            'caption' => 'Текст на изображении',
            'type'    => 'richtext',
        ],

        'image_text_color' => [
            'caption' => 'Цвет текста',
            'type'    => 'text',
            'default' => '#333',
        ],

        'autoshow_delay' => [
            'caption' => 'Задержка перед автоматическим показом, сек.',
            'type'    => 'text',
            'note'    => 'Если пусто, то не будет показываться',
        ],

        'letter_subject' => [
            'caption' => 'Тема письма',
            'type'    => 'text',
            'default' => 'Заявка!',
        ],
    ],

    'prepare' => function(&$options, &$values) {
        foreach ($values['fields'] as $index => &$field) {
            $field['required_markup'] = !empty($field['required'][0]) ? ' *' : '';

            if (!empty($field['type'])) {
                $field['field_markup'] = $this->parseTemplate($options['templates']['fields_markup'][$field['type']], ['index' => $index]);
            }
        }

        if (!empty($values['show_image'][0])) {
            $values['hasimage'] = ' with-image';
            $values['image_markup'] = trim($this->parseTemplate($options['templates']['image_markup'], $values));
        }

        $values['subtitle'] = trim($values['subtitle']);
        if (!empty($values['subtitle'])) {
            $values['subtitle'] = '<div class="modal-subtitle">' . $values['subtitle'] . '</div>';
        }

        if (!empty($values['autoshow_delay'])) {
            $values['script'] = $this->parseTemplate($options['templates']['script_markup'], [
                'alias' => $values['alias'],
                'delay' => intval($values['autoshow_delay']) * 1000,
            ]);
        }

        unset($field);
    },
];
