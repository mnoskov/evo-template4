<?php

return [
    'title' => 'Контакты',

    'show_in_templates' => $this->modx->templateConstants['contacts_template_id'],

    'placement' => 'content',

    'templates' => [
        'owner' => '
            <div class="contacts-items [+class+]">
                [+wrap+]
            </div>

            <div class="modal fade fullwidth" tabindex="-1" role="dialog" id="contactmap">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="close tpl-icon-close" data-dismiss="modal"></button>
                        <div class="frame"></div>
                    </div>
                </div>
            </div>
        ',
    ],

    'prepare' => function($options, &$values) {
        $values['placeholders']['class'] = count($values['items']) < 2 ? 'single' : '';
    },
];
