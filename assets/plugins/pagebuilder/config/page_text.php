<?php

return [
    'title' => 'Текст',

    'container' => 'page',

    'templates' => [
        'owner' => '
            <div class="user-content">
                [+richtext+]
            </div>
        ',
    ],

    'fields' => [
        'richtext' => [
            'caption' => 'Текст',
            'type'    => 'richtext',
            'theme'   => 'full',
            'options' => [
                'height' => '300px',
            ],
        ],
    ],
];
