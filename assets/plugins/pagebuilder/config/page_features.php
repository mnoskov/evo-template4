<?php

return [
    'title' => 'Преимущества',

    'container' => 'page',

    'templates' => [
        'owner' => '
            <div class="page-features">
                <ul class="items">
                    [+items+]
                </ul>
            </div>
        ',

        'items' => '
            <li>
                <div class="item">
                    <div class="icon"><img src="[+image+]"></div>
                    <div class="title">[+title+]</div>
                    [+text+]
                </div>
        ',
    ],

    'fields' => [
        'items' => [
            'caption' => 'Преимущества',
            'type'    => 'group',
            'layout'  => 'horizontal',
            'fields'  => [
                'title' => [
                    'caption' => 'Заголовок',
                    'type'    => 'text',
                ],

                'image' => [
                    'caption' => 'Изображение',
                    'type'    => 'image',
                ],

                'text' => [
                    'caption' => 'Текст',
                    'type'    => 'textarea',
                    'height'  => '60px',
                ],
            ],
        ],
    ],
];
