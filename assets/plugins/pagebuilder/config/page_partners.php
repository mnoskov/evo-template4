<?php

return [
    'title' => 'Партнеры',

    'container' => 'page',

    'templates' => [
        'owner' => '
            <div class="partners">
                <div class="slick" data-slick=\'{"arrows": true, "autoplay": true, "slidesToShow": 5, "responsive": [{"breakpoint": 991, "settings": {"slidesToShow": 4}}, {"breakpoint": 767, "settings": {"slidesToShow": 3}}, {"breakpoint": 575, "settings": {"slidesToShow": 2}}]}\'>
                    [+slides+]
                </div>
            </div>
        ',

        'slides' => '
            <span class="slide">
                <img src="[[phpthumb? &input=`[+image+]` &options=`w=150,h=100` &adBlockFix=`1`]]" alt="[+title+]">
            </span>
        ',
    ],

    'fields' => [
        'slides' => [
            'caption' => 'Логотипы',
            'type'    => 'group',
            'layout'  => 'gallery',
            'fields'  => [
                'title' => [
                    'caption' => 'Название',
                    'type'    => 'text',
                ],

                'image' => [
                    'caption' => 'Логотип',
                    'type'    => 'image',
                ],
            ],
        ],
    ],
];

