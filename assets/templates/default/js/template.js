slickDefaults.prevArrow = '<a href="#" class="ctrl prev"><i class="tpl-icon-arrow-left"></i></a>';
slickDefaults.nextArrow = '<a href="#" class="ctrl next"><i class="tpl-icon-arrow-right"></i></a>';

$('.fullscreen-slider .slick')
    .on('beforeChange', function(e, slick, current, next) {
        var $nextSlide = $(slick.$slides[next]);

        $nextSlide.removeClass('static');
        setTimeout(function() {
            $nextSlide.addClass('animated');
        });

        slick.prevSlide = $(slick.$slides[current]);

        $(document.body).toggleClass('inverted-slide', $nextSlide.attr('data-invert') == 1);
    })
    .on('init afterChange', function(e, slick, current) {
        if (slick.prevSlide) {
            slick.prevSlide.removeClass('animated');

            setTimeout(function() {
                slick.prevSlide.addClass('static');
            });
        } else if (!current) {
            var $slide = $(slick.$slides[slick.currentSlide]).addClass('animated');
            $(document.body).toggleClass('inverted-slide', $slide.attr('data-invert') == 1);
        }
    });

$(function() {
    var isMain = $(document.body).hasClass('main-page');

    (function($head, $menu, $getBack) {
        $(window)
            .on('scroll resize', function() {
                var height = $head.children('.head').outerHeight();
                if (!isMain) {
                    $head.css('height', height);
                }
                $menu.css('padding-top', height);

                if ($getBack.length) {
                    var $getb = $getBack.children('.get-back-fixed');
                    $getb.css('top', height);
                    height = $getb.outerHeight();
                    $getBack.css('height', height);
                }
            })
            .load(function() {
                $(this).scroll();
            })
            .scroll();
    })($('.head-placeholder'), $('.fixed-menu'), $('.get-back'));

    $('.accordeon').on('click', '.title', function(e) {
        e.preventDefault();

        var $item = $(this).closest('.item');

        if ($item.hasClass('active')) {
            $item.removeClass('active').find('.hidden').slideUp(200);
        } else {
            $item.addClass('active').find('.hidden').slideDown(200);
            $item.siblings('.active').removeClass('active').find('.hidden').slideUp(200);
        }

        setTimeout(function() {
            $('.product-info').hcSticky('refresh');
        }, 250);
    });

    /*$('.product-info').hcSticky({
        top: 210
        //column:  '.product-info',
        //content: '.product-images',
    });*/

    $('.toggle-search').click(function(e) {
        e.preventDefault();

        var $body = $(document.body);

        if (!$body.hasClass('main-page')) {
            $body.toggleClass('menu-opened');
        }

        setTimeout(function() {
            if ($body.hasClass('search-opened')) {
                $('.search-form input').focus();
            }
        }, 50);
    });

    $('.product-item').initializeProductItem();

    $('.faq-items .toggle').click(function(e) {
        e.preventDefault();
        $(this).next().slideToggle(200).closest('.item').toggleClass('active');
    });

    $('.contacts-items').each(function() {
        var $self = $(this);

        $self.find('.btn').click(function(e) {
            e.preventDefault();
            var $modal = $('#contactmap');
            $modal.find('.frame').html($(this).next('.map').attr('data-code'));
            $modal.modal('show');
        });

        if ($(this).hasClass('single')) {
            var $map = $(this).find('.map');
            $map.html($map.attr('data-code'));
            $self.find('.btn').remove();
        }
    });

    $(document).on('cart-add-complete.commerce', function (e, params) {
        if (params.response.status != 'success') {
            return;
        }

        var text, link;

        switch (params.data.instance) {
            case 'comparison':
                text = "Товар добавлен к сравнению";
                link = "/compare";
                break;
            case 'wishlist':
                text = "Товар добавлен в избранное";
                link = "/favorites";
                break;
            case 'products':
            default:
                text = "Товар добавлен в корзину";
                link = "/cart";
                break;
        }

        Toastify({
            text: text,
            destination: link,
            close: true
        }).showToast();
    });
});

var afterFilterComplete = function(form) {
    $('#eFiltr_results .product-item').initializeProductItem();
};

$.fn.initializeProductItem = function() {
    return this.each(function() {
        var $self = $(this),
            $image = $self.children('.image'),
            $preview = $image.children('.item-preview');

        if (!$preview.length) {
            return;
        }

        if ($preview.hasClass('preview-slider')) {
            $self.on('mousemove', function(e) {
                var slider = $(this).children('.image').children('.preview-slider').get(0);

                if (!slider.slick) {
                    return;
                }

                var slide = Math.floor(e.offsetX / this.offsetWidth * slider.slick.slideCount);

                if (slide != slider.slick.currentSlide) {
                    slider.slick.goTo(slide);
                }
            });
        } else {
            !function($preview) {
                if ($preview.attr('data-video').match(/youtube/)) {
                    $self.on('mouseover', function() {
                        if ($preview.hasClass('initialized')) {
                            $preview.children('iframe').each(function() {
                              this.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
                            });
                            return;
                        }

                        $preview.addClass('initialized');

                        $('<iframe frameborder="0" allow="autoplay"></iframe>')
                            .attr('src', $preview.attr('data-video') + '?rel=0&modestbranding=1&autohide=1&mute=1&showinfo=0&controls=0&autoplay=1&enablejsapi=1')
                            .appendTo($preview);
                    });

                    $self.on('mouseout', function(e) {
                        $preview.children('iframe').each(function() {
                          this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                        });
                    });
                } else {
                    $self.on('mouseover', function() {
                        if ($preview.video) {
                            $preview.video.play();
                            return;
                        }

                        var video = $('<video autoplay muted playsinline><source src="' + $preview.attr('data-video') + '" type="video/mp4"></video>').appendTo($preview).get(0);

                        $preview.video = video;
                    });

                    $self.on('mouseout', function() {
                        if ($preview.video) {
                            $preview.video.currentTime = 0;
                            $preview.video.pause();
                        }
                    });
                }
            }($preview);
        }
    });
};
