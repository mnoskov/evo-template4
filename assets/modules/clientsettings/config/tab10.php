<?php

return [
    'caption' => 'Информация о компании',
    'settings' => [
        'logo' => [
            'caption' => 'Логотип (для белого фона)',
            'type' => 'image',
        ],
        'logo_inverted' => [
            'caption' => 'Логотип (для темного фона)',
            'type' => 'image',
        ],
        'favicon' => [
            'caption' => 'Фавикон',
            'type' => 'image',
        ],
        'company_address' => [
            'caption' => 'Адрес компании',
            'type'  => 'text',
            'default_text' => 'г.Пермь, ул.Уличная, д.37, офис 142'
        ],
        'company_phone' => [
            'caption' => 'Контактный телефон компании',
            'type'  => 'text',
            'default_text' => '8 800 123 4567',
        ],
        'company_email' => [
            'caption' => 'Контактный email компании',
            'type'  => 'text',
            'default_text' => 'mail@' . $_SERVER['HTTP_HOST'],
        ],
        'policy' => [
            'caption' => 'Политика конфиденциальности',
            'type' => 'file',
        ],
        'preview_alg' => [
            'caption' => 'Тип предпросмотра товаров',
            'type' => 'dropdown',
            'elements' => 'Нет==none||Слайдер==slider||Видео==video',
            'default_text' => 'none',
        ],
    ],
];
